import { View, Text, StyleSheet, FlatList } from 'react-native'
import React from 'react'

const ChallengeFlatlist = () => {
    const netflixSeries = [

        {
            name: "Archive 81",
            year: "2022",
            Creator: "xyz",
            Genre: "Horror, Thriller",

        },

        {
            name: "Cheer",
            year: "2021-22",
            Creator: "abs",
            Genre: "Thriller, Drama",

        },

        {
            name: "Cobra Kai",
            year: "2018",
            Creator: "pqr",
            Genre: "Drama, Thriller",

        },

        {
            name: "The Witcher",
            year: "2022",
            Creator: "lmn",
            Genre: "Drama, Thriller",

        },

        {
            name: "Asoor",
            year: "2022",
            Creator: "xyz",
            Genre: "Horror, Thriller",

        },

        {
            name: "Dark",
            year: "2022",
            Creator: "xyz",
            Genre: "Horror, Thriller",

        },

        {
            name: "Death love",
            year: "2022",
            Creator: "xyz",
            Genre: "Horror, Thriller",

        },
    ];
    return (
        <View>
        <Text style={styles.textStylesOne}> List Of Top 10 Series in Netflix</Text>
        <FlatList
            keyExtractor={(key) => {
                return key.name;
            }}
            style={styles.listStyle}
            horizontal
            
            data={netflixSeries}
            renderItem={({ item }) => {
                console.log({ item })

                return (
                   
                        
                        <View style={styles.viewStyles}>

                            <Text style={styles.textStyles}>{item.name} </Text >
                            <Text style={styles.textStyles}>{item.Creator} </Text >
                            <Text style={styles.textStyles}>{item.year} </Text >
                            <Text style={styles.textStyles}>{item.Genre} </Text >
                        </View>
                   
                );
            }}
        />
    </View>
        
    );
};

const styles = StyleSheet.create({

    listStyle: {
        textAlign: "center",
        margin: 20,

        padding: 5,
        color: "white",

    },

    textStyles: {
        color: "white",
        backgroundColor: "#009688",

        fontSize: 30,
        textAlign: "center",

    },

    viewStyles: {
        padding: 5,
        margin: 20,


    },

    textStylesOne: {

        margin: 30,
        fontSize: 30,


    },



});

export default ChallengeFlatlist;